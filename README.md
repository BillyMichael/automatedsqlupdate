# Installation of prerequisites
- [Python 3.X](https://www.python.org/ftp/python/3.6.1/python-3.6.1.exe)
- Install the python ODBC module (pyodbc)
- Download the correct official OBDC driver for the type of database you wish to connect to by clicking the links below:
   - [MSSQL OBDC driver](https://www.microsoft.com/en-us/download/details.aspx?id=53339)
   - [MySQL OBDC Driver](https://dev.mysql.com/downloads/connector/odbc/)

# Installation & Configuration

## Installaing the pyodbc module

Python 3.x comes with pip preinstalled. This means you can open CMD and run the following command to install it

```
pip install --allow-external pyodbc --allow-unverified pyodbc pyodbc
```

## The configuration file

For added security the script comes with a configuration file which is not source controlled. To use the config carry out the following steps. 

- Create a copy of the config file (config-TEMPLATE.py) and rename it ```config.py```
- Fill in the variables for the database you wish to access
    - "server" is the hostname or IP of the database
    - "username" is the user you wish to connect to the database as
    - "password" is the password for the user
    - "port" will vary depending on your environment, the following are default.
        - MS SQL - 1433
        - MySQL - 3306
    - "driver" is the name of the OBDC driver you are using. If you used the links provided this should be the following:
        - MySQL OBDC 5.3 ANSI Driver
        - OBDC Driver 13 for SQL Server

## Compiling the code for security 

Once the configuration file has been updated with the correct details, cd to the folder with the scripts and compile the code using the following:

```
python -m py_compile .\automatedupdate.py
python -m py_compile .\config.py
```

Rename the compiled files to their original names

- automatedupdate.pyc
- config.pyc

## Upload the compiled code

Once this has all been completed upload the compiled code to the server and remove the sensitive information from the configuration file on your local machine.

## Scheduling the task

Windows task scheduler can be used to automate the task. To do this go to: Control Panel > System and Security > Administrative Tools > Task Scheduler.

Once in task scheduler do the following:

- Create Task
- General Tab
    - Enter a name
    - Select Run whether user is logged on or not
- Triggers Tab
    - Select how often you want the task to trigger e.g. Weekly, every 1 weeks on Monday
-  Actions
    - Program/script is the location of the python executable (Default=C:\Python36\python.exe)
    - Add Arguments is the name and location of the script you wish to run ("C:\Users\Billy Michael\Documents\Automated SQL Update\\\_\_pycache\_\_\\automatedupdate.pyc")

# Explaining the script

The script connects to the database using an OBDC connector which has been installed on the machine. To open the connection to the database the following command is run 

```python
"""Open the connection to database"""
connection = pyodbc.connect('DRIVER='+config.driver+';PORT='+config.port+';SERVER='+config.server+';DATABASE='+config.database+';UID='+config.username+';PWD='+config.password+';OPTION=3')
```

Once connected to the database it retrieves the version number by querying the version table and returning the result

```python
"""Return the curson object"""
cursor = connection.cursor()
cursor.execute('select version from dbo.verssion')
currentversion = cursor.fetchone()[0]
cursor.close()

"""Set current version to new version"""
newversion = currentversion
```

The script then begins to iterate over the files to check for files which have a newer version than the current one. To do this it uses a regular expression to check the number at the beginning of each file.

If the script is newer than the current version it then iterates through each query within the file by splitting on ```";"``` and executes each query individually, closing the cursor each time

Finally, it stores what the current highest "new version" is.

```python

"""
Iterate over files in the scripts folder checking their version number 
against the version number stored in the database

If the script is newer than the version in the database then the queries
in the .sql file will be split and executed individually.

The newest version used is then stored
"""
for files in os.listdir(scriptsfolder):
    version = int(re.search('^[0-9]*', files)[0])
    if version > currentversion:
        with open(scriptsfolder + '/' + files, 'r') as inserts:
            script = " ".join(line.strip() for line in inserts)
            for statement in script.split(';'):
                if len(statement) > 1:
                    with connection.cursor() as cur:
                        try:
                            cur.execute(statement)
                        except pyodbc.Error as err:
                            logger.warning(err)
        if version > newversion:
            newversion = version
```

Once all the files have been checked the script runs a query to update the version table with the newest version which has been run. It also adds a line to a log file and closes the coonection to the database

```python
"""
If a newer version was found and updates were carried out to the database this
will update the version table to the newest version number
"""
if newversion > currentversion:
    with connection.cursor() as cur:
        cur.execute('UPDATE '+versiontable+' SET version='+str(newversion)+\
        ' WHERE version='+str(currentversion)+';')
    logger.info('Updated from version '+str(currentversion)+' to '+str(newversion))
else:
    logger.info('No updates have been completed. DB remains at Version '+str(currentversion))

"""
Close the connection to the database before terminating the script
"""
connection.commit()
connection.close()
```

# Troubleshooting
The following are some common errors you may encounter:

---
When installing the pyobdc you could encounter the following error:

```
Collecting pyobdc
Could not find a version that satisfies the requirement pyobdc (from versions: )
No matching distribution found for pyobdc
```

This error is caused because you did not run the command given in "Installation of prerequisites" to install pyobdc.

pyobdc is not listed in the default PyPi repository. This means that you must allow external repositories to install it. This involves the use of ```--allow-external``` and ```allow-unverified```.

To successfully install the module please use the following command

```
pip install --allow-external pyodbc --allow-unverified pyodbc pyodbc
```

---

When running the script you may get the following error

```python
pyodbc.Error: ('IM002', '[IM002] [Microsoft][ODBC Driver Manager] Data source name not found and no default driver specified (0) (SQLDriverConnect)')
```

This is an OBDC driver error which is caused by stating the incorrect driver on line 12. To check the exact name of the driver carry out the following steps:

- Open the OBDC administration by going to Control Panel > System and Security > Administrative Tools > OBDC Data Sources (64-Bit)
- Click the "Drivers" tab
- Check the name column. If you used the download links provided these should be:
    - MySQL OBDC 5.3 ANSI Driver
    - OBDC Driver 13 for SQL Server
- Enter the exact name in the driver variable

# Notes

## Note 1

I decided to split the configuration parameters into a separate file which is in the .gitignore file. Alternately these parameters could be stored in the main script to minimise the number of scripts which are needed. 

My reason for separating them is to prevent anyone working on the script from accidentally pushing sensitive information to the git repository.

## Note 2

The compiling of the code was also done for added security. This minimises the risk of anyone with access to the servers being able to few the login credentials. This may not be something which is necessary for a client but was added to demonstrate the importance of security.

---

# Changelog

## 1.0.0

Initial release of the script 

## 1.1.0

The following bug fixes and additions were added.

- Correct incorrect version table name in final version update query.
- Fixed an issue which lead to multi line queries not working.
- Terminated the database connection once the script has finished running.
- Simplified the cursor creation and close.
- Increased the test sample size to include:
    - More .sql files
    - A wider range of .sql file naming conventions
    - Multi line functions
- Added a log file to track updates
- Failed queries will get printed to the log