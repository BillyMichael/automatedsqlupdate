import os
import re
import logging
import pyodbc
import config

"""
Configure logging
"""
logger = logging.getLogger('SQL_Update')
logger.setLevel(logging.INFO)
hdlr = logging.FileHandler('sqlupgrade.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 

"""Initialise folders & tables"""
scriptsfolder = 'C:/Users/Billy Michael/Documents/bitbucket/automatedsqlupdate/.sql files'
logfile = 'C:/Users/Billy Michael/Documents/bitbucket/automatedsqlupdate/log.txt'
versiontable = 'dbo.version'

"""Open the connection to database"""
connection = pyodbc.connect('DRIVER='+config.driver+';PORT='+config.port+';SERVER='+config.server+\
';DATABASE='+config.database+';UID='+config.username+';PWD='+config.password+';OPTION=3', autocommit=False)

"""Return the curson object"""
cursor = connection.cursor()
cursor.execute('select version from '+versiontable)
currentversion = cursor.fetchone()[0]
cursor.close()

"""Set current version to new version"""
newversion = currentversion

"""
Iterate over files in the scripts folder checking their version number 
against the version number stored in the database

If the script is newer than the version in the database then the queries
in the .sql file will be split and executed individually.

The newest version used is then stored
"""
for files in os.listdir(scriptsfolder):
    version = int(re.search('^[0-9]*', files)[0])
    if version > currentversion:
        with open(scriptsfolder + '/' + files, 'r') as inserts:
            script = " ".join(line.strip() for line in inserts)
            for statement in script.split(';'):
                if len(statement) > 1:
                    with connection.cursor() as cur:
                        try:
                            cur.execute(statement)
                        except pyodbc.Error as err:
                            logger.warning(err)
        if version > newversion:
            newversion = version

"""
If a newer version was found and updates were carried out to the database this
will update the version table to the newest version number
"""
if newversion > currentversion:
    with connection.cursor() as cur:
        cur.execute('UPDATE '+versiontable+' SET version='+str(newversion)+\
        ' WHERE version='+str(currentversion)+';')
    logger.info('Updated from version '+str(currentversion)+' to '+str(newversion))
else:
    logger.info('No updates have been completed. DB remains at Version '+str(currentversion))

"""
Close the connection to the database before terminating the script
"""
connection.commit()
connection.close()
